package com.mavenpackage.MavenTest1;



import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class FirstScenario1 {
	WebDriver driver;
	
    Properties prop = new Properties();
	@Test
	public void automationpractice1() throws InterruptedException, IOException {
		
		FileInputStream fis = new FileInputStream( System.getProperty("user.dir")+"\\Properties\\config.properties");
		prop.load(fis);
		
	//System.setProperty("webdriver.chrome.driver", "C:\\Users\\cedri\\Desktop\\Updated Chromedriver Location\\chromedriver.exe");
		//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
		String GenericChromePath = System.getProperty("user.dir")+"\\Driver\\chromedriver.exe";
	    System.setProperty("webdriver.chrome.driver",GenericChromePath);
        
	    driver = new ChromeDriver();
	    
	    //driver.get("http://automationpractice.com/");
		
	    driver.get(prop.getProperty("QA_Url"));
		
		driver.manage().window().maximize(); // maximize the window
		
		System.out.println(driver.getTitle()); //get the title of website
		
		
		//WebElement Women =driver.findElement(By.xpath("//a[text()='Women']"));
		WebElement Women =driver.findElement(By.xpath(prop.getProperty("Women_Xpath")));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(Women).perform();
		
		Thread.sleep(5000);
		
		System.out.println("Hover on women");
		
		driver.findElement(By.xpath(prop.getProperty("CasualDress_Xpath"))).click();
       
		Thread.sleep(5000);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,800)");
		
        WebElement inStock = driver.findElement(By.xpath(prop.getProperty("Instock_Xpath")));
        
       System.out.println("User on InStock xpath");
		
		act.moveToElement(inStock).perform();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath(prop.getProperty("QuickView_Xpath"))).click(); 
		
		System.out.println("Quick view xpath");
		
		Thread.sleep(3000);
		driver.switchTo().frame(0); 
		
		System.out.println("enter in to frame");
		
		driver.findElement(By.id(prop.getProperty("Quantity_Id"))).clear();
		Thread.sleep(5000);
		driver.findElement(By.id(prop.getProperty("Quantity_Id"))).sendKeys("4");
		Thread.sleep(5000);
		
		
		
		driver.findElement(By.xpath(prop.getProperty("AddtoCart_Xpath"))).click();
		
		System.out.println("Add to cart");
		driver.switchTo().defaultContent(); //come out from the frame
		
		System.out.println("switch frame");
		
		Thread.sleep(8000);
		
		String expectedPrice= driver.findElement(By.xpath(prop.getProperty("ExpectedPrice_Xpath"))).getText();
		System.out.println("Expected Price is "+expectedPrice);
		
		
		
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("FirstProceed_Xpath"))).click();
		
        Thread.sleep(5000);
		
		String totalExpectedPrice= driver.findElement(By.xpath(prop.getProperty("TotalPrice_Xpath"))).getText();
		System.out.println("Total Expected Price is "+totalExpectedPrice);
		
		
		Thread.sleep(10000);
		driver.findElement(By.xpath(prop.getProperty("SecondProceed_Xpath"))).click();
	}
		
		@Test
		public void automationpractice2() throws InterruptedException {
			
			driver.findElement(By.xpath(prop.getProperty("Username_Xpath"))).sendKeys("abc1111111@gmail.com");
			driver.findElement(By.xpath(prop.getProperty("Password_Xpath"))).sendKeys("123456");
			
			Thread.sleep(2000);
			driver.findElement(By.xpath(prop.getProperty("Submit_Xpath"))).click();
			Thread.sleep(2000);
		}
			
			@Test
			public void automationpractice3() throws InterruptedException {
				
				driver.findElement(By.xpath(prop.getProperty("ThirdProceed_Xpath"))).click();
				driver.findElement(By.xpath(prop.getProperty("Checkbox_Xpath"))).click();
				driver.findElement(By.xpath(prop.getProperty("FourthProceed_Xpath"))).click();
				driver.findElement(By.xpath(prop.getProperty("BankWirePayment_Xpath"))).click();
				driver.findElement(By.xpath(prop.getProperty("OrderConfirmation_Xpath"))).click();
				
				Thread.sleep(3000);
				
				String finalExpectedPrice= driver.findElement(By.xpath(prop.getProperty("FinalPrice_Xpath"))).getText();
				System.out.println("Final Expected Price is "+finalExpectedPrice);

}
			@Test
			public void automationpractice4() throws InterruptedException {
				driver.findElement(By.xpath(prop.getProperty("ContactUs_Xpath"))).click();
				Thread.sleep(3000);
				WebElement selbox = driver.findElement(By.id(prop.getProperty("SubjectHeadingDropdown_Id")));
				
				Select sel = new Select(selbox);
				
				sel.selectByVisibleText("Customer service");
				Thread.sleep(3000);
				
WebElement selbox2 = driver.findElement(By.xpath(prop.getProperty("OrderReference_Xpath")));
				
				Select sel2 = new Select(selbox2);
				
				sel2.selectByVisibleText("UUEDMGGXJ - 12/22/2021");
				
WebElement selbox3 = driver.findElement(By.id(prop.getProperty("Product_Id")));
				
				Select sel3 = new Select(selbox3);
				sel3.selectByVisibleText("Printed Dress - Color : Orange, Size : S");		
				
				Thread.sleep(3000);
				driver.findElement(By.id(prop.getProperty("Message_Id"))).sendKeys("The dress is not of good quality");
				Thread.sleep(3000);
				driver.findElement(By.xpath(prop.getProperty("Send_Xpath"))).click();
				
				
			}
}